// page init
jQuery(function(){
	/* ==========================================
scrollTop() >= 300
Should be equal the the height of the header
========================================== */

    $(window).scroll(function(){
        if ($(window).scrollTop() >= 100) {
            $('#header').addClass('fixed');
            // $('nav div').addClass('visible-title');
        }
        else {
            $('#header').removeClass('fixed');
            // $('nav div').removeClass('visible-title');
        }
    });


    $(".view-hide-link strong").click(function() {
        $(this).toggleClass("active");
        $(this).parent('.view-hide-link').parent('.project').find('.expanded-gallery').toggle('');
    });
});